package gt.com.masdigital.myfirstkotlin

data class ItemLanding(val title: String, val desc: String, val price: Double)