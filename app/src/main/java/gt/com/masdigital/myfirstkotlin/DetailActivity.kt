package gt.com.masdigital.myfirstkotlin

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.content_detail.*
import org.jetbrains.anko.db.insert
import org.jetbrains.anko.startActivity

class DetailActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        val db = DBOpenHelper.getInstance(this)

        intent?.extras?.let {
            val name = it.getString("title")
            val desc = it.getString("descrp")
            val price = it.getDouble("price")
            tvProductName.text = name
            tvProductDescrp.text = desc
            tvProductPrice.text = price.toString()

            btnBuy.setOnClickListener {
                db?.use {
                    val nameProduct = "name" to name
                    val descripProduct = "desc" to desc
                    val priceProduct = "price" to price
                    insert("Productos", nameProduct, descripProduct, priceProduct)
                }
                startActivity<ShopCartActivity>()
            }
        }
    }
}
